<?php

/**
* change plain number to formatted currency
*
* @param $number
* @param $currency
*/
function formatPrice($number, $currency = 'EUR')
{
  	switch($currency){
  		case 'EUR':
  			return 'EUR '.number_format($number,2,',','.');
  		break;
  	}
}


function print_rr($array){
	echo '<pre>';
	print_r($array);
	echo '</pre>';
}

function getLangContent($obj,$entity,$content,$lang){
	$index = implode('_',[$entity,$content,$lang]);
	return $obj->$index;
}

function fixDate($date){
	return date('Y-m-d',strtotime($date));
}

function setArrayDefaults($array,$defaults){
	foreach($defaults as $index => $value){
		$array[$index] = (!empty($array[$index])) ? $array[$index] : $value ; 
	}
	return $array;
}


?>