<?php namespace App\Http\Controllers;

use Session;
use Request;
use DB;
use CRUDbooster;

class AdminCmsUsersController extends \crocodicstudio\crudbooster\controllers\CBController {


	public function cbInit() {
		# START CONFIGURATION DO NOT REMOVE THIS LINE
		$this->table               = 'cms_users';
		$this->primary_key         = 'id';
		$this->title_field         = "name";
		$this->button_action_style = 'button_icon';	
		$this->button_import 	   = FALSE;	
		$this->button_export 	   = FALSE;	
		# END CONFIGURATION DO NOT REMOVE THIS LINE
	
		# START COLUMNS DO NOT REMOVE THIS LINE
		$this->col = array();
		$this->col[] = array("label"=>"Photo","name"=>"photo","image"=>1);	
		$this->col[] = array("label"=>"Name","name"=>"name");
		$this->col[] = array("label"=>"Surname","name"=>"surname");
		$this->col[] = array("label"=>"Address","name"=>"address");
		$this->col[] = array("label"=>"Telephone","name"=>"telephone");
		$this->col[] = array("label"=>"Email","name"=>"email");
		$this->col[] = array("label"=>"Privilege","name"=>"id_cms_privileges","join"=>"cms_privileges,name");
		$this->col[] = array("label"=>"Horoscope Sign","name"=>"horoscope_signs_id","join"=>"horoscope_signs,horoscope_sign_name_en_us");
		$this->col[] = array("label"=>"Profession","name"=>"professions_id","join"=>"professions,profession_name_en_us");
		$this->col[] = array("label"=>"Gender","name"=>"genders_id","join"=>"genders,gender_name_en_us");
		# END COLUMNS DO NOT REMOVE THIS LINE

		# START FORM DO NOT REMOVE THIS LINE
		$this->form = array(); 		
		$this->form[] = array("label"=>"Name","name"=>"name",'required'=>true,'validation'=>'required|alpha_spaces|min:3');
		$this->form[] = array("label"=>"Surname","name"=>"surname",'required'=>true,'validation'=>'required');
		$this->form[] = array("label"=>"Address","name"=>"address",'validation'=>'');
		$this->form[] = array("label"=>"Telephone","name"=>"telephone",'required'=>true,'validation'=>'required');
		$this->form[] = array("label"=>"Email","name"=>"email",'required'=>true,'type'=>'email','validation'=>'required|email|unique:cms_users,email,'.CRUDBooster::getCurrentId());		
		$this->form[] = array("label"=>"Photo","name"=>"photo","type"=>"upload","help"=>"Recommended resolution is 200x200px",'required'=>true,'validation'=>'required|image|max:1000','resize_width'=>90,'resize_height'=>90);											
		$this->form[] = array("label"=>"Privilege","name"=>"id_cms_privileges","type"=>"select","datatable"=>"cms_privileges,name",'required'=>true);						
		$this->form[] = array("label"=>"Password","name"=>"password","type"=>"password","help"=>"Please leave empty if not change");
		$this->form[] = array("label"=>"Horoscope Sign","name"=>"horoscope_signs_id","type"=>"select","datatable"=>"horoscope_signs,horoscope_sign_name_en_us");
		$this->form[] = array("label"=>"Profession","name"=>"professions_id","type"=>"select","datatable"=>"professions,profession_name_en_us");
		$this->form[] = array("label"=>"Gender","name"=>"genders_id","type"=>"select","datatable"=>"genders,gender_name_en_us");
		# END FORM DO NOT REMOVE THIS LINE
				
	}

	public function getProfile() {			

		$this->button_addmore = FALSE;
		$this->button_cancel  = FALSE;
		$this->button_show    = FALSE;			
		$this->button_add     = FALSE;
		$this->button_delete  = FALSE;	
		$this->hide_form 	  = ['id_cms_privileges'];

		$data['page_title'] = trans("crudbooster.label_button_profile");
		$data['row']        = CRUDBooster::first('cms_users',CRUDBooster::myId());		
		$this->cbView('crudbooster::default.form',$data);				
	}
}
