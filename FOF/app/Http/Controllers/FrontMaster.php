<?php namespace App\Http\Controllers;

use Session;
use Request;
use DB;
use CRUDBooster;
use App;

class FrontMaster extends Controller
{
	var $filters;
	var $room_types;
	var $data;
	var $days_num;

	public function __construct(){
		$currentLang = 'en_us';
		$this->filters =  Request::__get('filters');
		$this->filters = setArrayDefaults($this->filters,[
			'check_in' => date('Y-m-d'),
			'check_out' => date('Y-m-d',strtotime('+3months')),
			'room_adults' => 2,
			'rooms_quantity' => 1,
			'months'=>2
		]);
		$room_types_ = DB::table('room_types')->get();
		
		foreach($room_types_ as $index=> $room_type){
			$this->room_types[$index] = $room_type;
			$this->room_types[$index]->room_type_name =  getLangContent($room_type,'room','type_name',$currentLang);
		}
		
    	$date_start = date_create($this->filters['check_in']);
    	$date_end = date_create($this->filters['check_out']);
    	$diff  	= date_diff( $date_start, $date_end );

    	$zones_ = DB::table('zones')->get();
    	foreach($zones_ as $index => $zone){
    		$zones[$index] = $zone;
    		$zones[$index]->zone_name = getLangContent($zone,'zone','name',$currentLang);
    	}

    	
    	if($diff->format('%m')>=3){
	    	$this->days_num = $diff->days;
	    	$this->months_num = $diff->format('%m');
	    	$this->data['days_num'] = $diff->days;
			$this->data['filters'] = $this->filters;
			$this->data['room_types'] = $this->room_types;
			$this->data['zones'] = $zones;
			$this->data['search_form_action'] = route('search',['locale'=>$currentLang]);
			#print_rr($this->filters);
		}else{
			//return back()->withInput();
			header('location: '.$_SERVER['HTTP_REFERER']);
			exit;
		}
	}
}

/*

HTTP/1.0 302 Found Cache-Control: no-cache, private Date: Thu, 23 Aug 2018 13:08:36 GMT Location: http://localhost:8000/search?filters%5Bcheck_in%5D=2018-08-25&filters%5Bcheck_out%5D=2018-09-31&filters%5Broom_adults%5D=2&filters%5Brooms_quantity%5D=1 Redirecting to http://localhost:8000/search?filters%5Bcheck_in%5D=2018-08-25&filters%5Bcheck_out%5D=2018-09-31&filters%5Broom_adults%5D=2&filters%5Brooms_quantity%5D=1.

*/

