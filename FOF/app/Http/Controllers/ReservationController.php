<?php namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Session;
use DB;
use CRUDBooster;
use App;
use Stripe\Error\Card;
use Cartalyst\Stripe\Stripe;
use Input;

class ReservationController extends FrontMaster
{
    /**
     * Show the profile for the given user.
     *
     * @param  int  $id
     * @return Response
     */
    public function reserve ($locale,$rooms_id) {
      $currentLang = 'en_us';
    	App::setLocale($locale);
        $front = new FrontMaster();
        $room = new Room($rooms_id);
        $data = $front->data;
        $data['room'] = $room->room;

        $horoscope_signs_ = DB::table('horoscope_signs')->get();
        foreach($horoscope_signs_ as $index => $sign){
          $horoscope_signs[$index] = $sign;
          $horoscope_signs[$index]->horoscope_sign_name = getLangContent($sign,'horoscope_sign','name',$currentLang);
        }
        $data['horoscope_signs'] = $horoscope_signs;

        $genders_ = DB::table('genders')->get();
        foreach($genders_ as $index => $gender){
          $genders[$index] = $gender;
          $genders[$index]->gender_name = getLangContent($gender,'gender','name',$currentLang);
        }
        $data['genders'] = $genders;

         $professions_ = DB::table('professions')->get();
        foreach($professions_ as $index => $profession){
          $professions[$index] = $profession;
          $professions[$index]->profession_name = getLangContent($profession,'profession','name',$currentLang);
        }
        $data['professions'] = $professions;
        
        

        return view('reserve',$data);
    }

    public function payWithStripe(){

    }
    public function postPaymentWithStripe(){
      
    }

    public function print_rr($array){
      echo '<pre>';
      print_r($array);
      echo '</pre>';
    }

    public function finalizeReservation(Request $request){
        $input = $request -> all();
        $room = new Room($input['reservation']['rooms_id']);
        $room = $room->room;
        


        $reservation_total = $room->total_with_commissions;
        
        $input['reservation']['cms_users_id'] = DB::table('cms_users')->insertGetId($input['user']);
        $input['reservation']['reservation_from'] = $input['filters']['check_in'];
        $input['reservation']['reservation_to'] = $input['filters']['check_out'];
        $input['reservation']['reservation_total'] = $reservation_total;
        $reservations_id = DB::table('reservations')->insertGetId($input['reservation']);
        $card = $input['card'];
         $stripe = Stripe::make('sk_test_Q6XySoNP4mItkQ84rhJmpONL');
        try {
           $token = $stripe->tokens()->create([
               'card' => [
                   'number' => $card['card_no'],
                   'exp_month' => $card['ccExpiryMonth'],
                   'exp_year' => $card['ccExpiryYear'],
                   'cvc' => $card['cvvNumber'],
               ],
           ]);
        
 // $token = $stripe->tokens()->create([
 // 'card' => [
 // 'number' => '4242424242424242',
 // 'exp_month' => 10,
 // 'cvc' => 314,
 // 'exp_year' => 2020,
 // ],
 // ]);
           if (!isset($token['id'])) {
               echo 'no token set';
               exit;
           }
           $charge = $stripe->charges()->create([
               'card' => $token['id'],
               'currency' => 'EUR',
               'amount' => $reservation_total,
               'description' => 'Add in wallet',
           ]);

           if($charge['status'] == 'succeeded') {
               echo '<pre>';
               print_r($charge);
               exit;
           }else{
            $this->print_rr($charge);
           }
       }   
       catch(\Cartalyst\Stripe\Exception\CardErrorException $e) {
           \Session::put('error',$e->getMessage());
           return redirect()->route('addmoney.paywithstripe');
       } 
       catch(\Cartalyst\Stripe\Exception\MissingParameterException $e) {
           \Session::put('error',$e->getMessage());
           return redirect()->route('addmoney.paywithstripe');
       }        
   }
}