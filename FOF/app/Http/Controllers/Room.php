<?php namespace App\Http\Controllers;

use Session;
use Request;
use DB;
use CRUDBooster;
use App;

class Room extends Controller{
    var $room;
    public function __construct($rooms_id){

        $today = date('Y-m-d');

        $front = new FrontMaster();

        $filters =  Request::__get('filters');

        

        $check_in = fixDate($filters['check_in']);
        $check_out = fixDate($filters['check_out']);



        $currentLang = 'en_us';
        $data = $front->data;

        $days_num = $front->days_num;
        $room = DB::table('rooms')
        ->select(
            'rooms.*',
            'rooms.id as rooms_id',
            'apartments.*',
            'room_types.*',
            'apartments.id as apartments_id',
            'apartment_types.*',
            DB::raw('(SELECT GROUP_CONCAT(photo_path) FROM photos WHERE photos.rooms_id = rooms.id GROUP BY photos.rooms_id) as room_photos')
        )
        ->join('room_types','room_types.id','=','rooms.room_types_id')
        ->join('apartments','apartments.id','=','rooms.apartments_id')
        ->join('apartment_types','apartment_types.id','=','apartments.apartment_types_id')

        ->where('rooms.id',$rooms_id)
        ->first();

        $other_rooms_ = DB::table('rooms')->where('rooms.apartments_id',$room->apartments_id)->where('rooms.id','!=',$rooms_id)->get();

        if(count($other_rooms_)){
            foreach($other_rooms_ as $index => $other_room){
                
                $other_rooms[$index] = $other_room;
                $reservation_ =  DB::table('reservations')
                    ->join('cms_users','cms_users.id','=','reservations.cms_users_id')
                    ->leftJoin('horoscope_signs','horoscope_signs.id','=','cms_users.horoscope_signs_id')
                    ->leftJoin('genders','genders.id','=','cms_users.genders_id')
                    ->leftJoin('professions','professions.id','=','cms_users.professions_id')
                    ->where('rooms_id',$other_room->id)
                    ->first();
                #print_rr($reservation_);
                #continue;
                if($reservation_){
                    $reservation = $reservation_;
                    $reservation->profession_name = getLangContent($reservation_,'profession','name',$currentLang);
                    $reservation->horoscope_sign_name = getLangContent($reservation_,'horoscope_sign','name',$currentLang);
                    $reservation->gender_name = getLangContent($reservation_,'gender','name',$currentLang);
                    $reservation->checked_in = ($reservation_->reservation_from>=$today) ? true : false ; 

                    $other_rooms[$index]->reservation = $reservation;
                   }else{
                    $other_rooms[$index]->reservation = [];
                   }
                $other_rooms[$index]->room_description = getLangContent($other_room,'room','description',$currentLang);
                $other_rooms[$index]->room_name = getLangContent($other_room,'room','name',$currentLang);
            }
        }else{
            $other_rooms = [];
        }
        
        $services_ = 
        DB::table('services')
        ->join('rooms_services','services.id','=','rooms_services.services_id')
        ->where('rooms_services.rooms_id',$room->rooms_id)->get();
        if(count($services_)){
            foreach($services_ as $index => $service){
                $services[$index] = $service;
                $services[$index]->service_name = getLangContent($service,'services','name',$currentLang);
            }
        }
        else{
            $services = [];
        }

        $deposit_months = [];
        $deposit_months[1] = 0;
        $deposit_months[2] = 0;
        $deposit_months[3] = 0;
        $deposit_months[4] = 1;
        $deposit_months[5] = 1;
        $deposit_months[6] = 1;
        $deposit_months[7] = 1;
        $deposit_months[8] = 1;
        $deposit_months[9] = 2;
        $deposit_months[10] = 2;
        $deposit_months[11] = 2;
        $deposit_months[12] = 2;
        $prices = [];

        $room->services = $services;
        $room->other_rooms = $other_rooms;
        $room->deposit_months = $deposit_months[$filters['months']];
        $room->room_photos = explode(',',$room->room_photos);
        $room->price =  30*$room->room_medium_price;#set temporarily to mid price 
        $room->price_total = 30*$room->room_medium_price*$filters['months'];
        $room->room_description = getLangContent($room,'room','description',$currentLang);
        $room->apartment_type = getLangContent($room,'apartment','type_name',$currentLang);
        $room->room_type = getLangContent($room,'room','type_name',$currentLang);	
        $room->coordinates_json = json_encode((object)['lat'=>(float)substr($room->apartment_lat,0, 8),'lng'=>(float)substr($room->apartment_lng,0, 8)]);
        $room->deposit = 30*$room->room_medium_price*$deposit_months[$filters['months']];
        $room->months_commission = 22-$filters['months'];   
        $room->commission_total = $room->price_total*$room->months_commission/100;
        $room->total_with_commissions = $room->price_total+$room->commission_total;
        


        $this->room = $room;
        
    }
}

?>