<?php namespace App\Http\Controllers;

use Session;
use Request;
use DB;
use CRUDBooster;
use App;


class RoomController extends Controller
{

    public function view ($locale,$rooms_id) {
        App::setLocale($locale);
        $front = new FrontMaster();
        $room = new Room($rooms_id);

        $data = $front->data;
        
        $data['room'] = $room->room;
        
        return view('room',$data);
    }
}
?>