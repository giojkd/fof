<?php namespace App\Http\Controllers;

use Session;
use Request;
use DB;
use CRUDBooster;
use App;

class SearchController extends FrontMaster
{
    /**
     * Show the profile for the given user.
     *
     * @param  int  $id
     * @return Response
     */
    public function search ($locale='en_us') {

    	App::setLocale($locale);

        $data = $this->data;
        $days_num = $this->days_num;

    	$check_in = fixDate($filters['check_in']);
    	$check_out = fixDate($filters['check_out']);

    	$currentLang = 'en_us';

    	$apartments_ = DB::table('apartments')
    	->join('rooms','apartments.id','=','rooms.apartments_id')
	#->leftJoin('room_types','rooms.room_types_id','=','room_types_id')
    	->select(
    		'rooms.*'
            ,'rooms.id as rooms_id'
    		,'apartments.*'
		#,'room_types.*'
    		,DB::raw('(SELECT GROUP_CONCAT(photo_path) FROM photos WHERE photos.rooms_id = rooms.id GROUP BY photos.rooms_id) as room_photos')
    		,DB::raw('(SELECT room_type_name_'.$currentLang.' FROM room_types WHERE room_types.id = rooms.room_types_id) as room_type_name'))
    	->whereRaw('rooms.id NOT IN(
    		SELECT rooms_id 
    		FROM reservations 
    		WHERE 
    		(reservation_from BETWEEN \''.$check_in.'\' AND \''.$check_out.'\')
    		OR
    		(reservation_to BETWEEN \''.$check_in.'\' AND \''.$check_out.'\')
    		OR
    		(reservation_from > \''.$check_in.'\' AND \''.$check_out.'\' > reservation_to)
    		OR
    		(\''.$check_in.'\' > reservation_from  AND reservation_to > \''.$check_out.'\')
    		GROUP BY rooms_id
    	)')
        #temporarily
    	->get();

    	

    	if(count($apartments_)){
    		foreach($apartments_ as $index => $apartment){
    			$apartments[$index] = $apartment;
    			$apartments[$index]->room_photos = explode(',',$apartment->room_photos);
    			$apartments[$index]->apartment_description = getLangContent($apartment,'apartment','description',$currentLang);
    			$apartments[$index]->room_description = getLangContent($apartment,'room','description',$currentLang);
    			$apartments[$index]->price = 30*$apartment->room_medium_price;#set temporarily to mid price 
    			$estate_coordinates[] = 
    			[
    				'id'=>$apartment->rooms_id,
    				'lat'=>substr($apartment->apartment_lat,0, 8),
    				'lng'=>substr($apartment->apartment_lng,0, 8)
    			];
    		}
    	}

        
    	$data['apartments'] = $apartments;
    	$data['estate_coordinates_json'] = json_encode($estate_coordinates);
        


    	return view('home',$data);
    }
}
?>