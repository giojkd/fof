<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRoomsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rooms', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            
            $table->string('room_name',100)->nullable();
            $table->text('room_description_en_us')->nullable();

            $table->integer('room_adults')->nullable();
            $table->integer('room_kids')->nullable();
            $table->integer('room_infants')->nullable();
            
      
            $table->float('room_min_price')->nullable();
            $table->float('room_medium_price')->nullable();
            $table->float('room_high_price')->nullable();
            #foreign keys
            $table->integer('room_types_id')->nullable();#entire apartment, room, shared room

            $table->integer('ensuite_bathroom')->nullable();

            $table->integer('apartments_id');

            $table->integer('room_surface')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rooms');
    }
}


/*


*/
