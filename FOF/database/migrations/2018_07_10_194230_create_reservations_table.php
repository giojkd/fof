<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReservationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reservations', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->date('reservation_from')->nullable();
            $table->date('reservation_to')->nullable();
            
            $table->integer('reservation_status_id')->nullable();
            $table->float('reservation_total')->nullable();
            $table->dateTime('reservation_confirmation_date')->nullable();
            #foreign keys
            $table->integer('cms_users_id')->nullable(); #the user who reserved
            $table->integer('rooms_id')->nullable();#room reserved
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reservations');
    }
}
