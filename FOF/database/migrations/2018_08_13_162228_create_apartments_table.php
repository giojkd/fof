<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateApartmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('apartments', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->integer('cms_users_id')->nullable();#landlord

            $table->string('apartment_name',100)->nullable();
            $table->text('apartment_description_en_us')->nullable();

            $table->string('apartment_events_accepted',100)->nullable();
            $table->string('apartment_smoking_accepted',100)->nullable();
            $table->string('apartment_pets_accepted',100)->nullable();

            $table->string('apartment_full_address',100)->nullable();
            $table->string('apartment_administrative_area_level_1',100)->nullable();
            $table->string('apartment_administrative_area_level_2',100)->nullable();
            $table->string('apartment_administrative_area_level_3',100)->nullable();
            $table->string('apartment_locality',100)->nullable();
            $table->string('apartment_postal_code',100)->nullable();
            $table->string('apartment_route',100)->nullable();
            $table->string('apartment_street_number',100)->nullable();
            $table->string('apartment_country',100)->nullable();

            $table->integer('apartment_rooms_count')->nullable();
            $table->integer('apartment_beds_count')->nullable();
            $table->integer('apartment_bathapartments_count')->nullable();

            $table->string('apartment_lat',100)->nullable();
            $table->string('apartment_lng',100)->nullable();

            $table->integer('apartment_types_id')->nullable();

            $table->integer('zones_id')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('apartments');
    }
}
