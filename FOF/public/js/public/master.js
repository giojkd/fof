function updateDates(){
		var date_from = moment($('#date-check-in').val());
		var months = parseInt($('select[name="filters[months]"]').val());
		var date_to = date_from.add(months, 'months');
		$('#date-check-out').val(date_to.format('YYYY-MM-DD'));
	}
$(function(){

	$('select[name="filters[months]"]').change(function(){updateDates()});
	$('#daterangepicker').daterangepicker({
		singleDatePicker: true,
		opens: 'left',
		minDate:moment(),
		locale: {
      		format: 'YYYY-MM-DD',
    	},
    	"autoApply": true,
	}, function(start, end, label) {
  			$('#date-check-in').val(start.format('YYYY-MM-DD'));
  			updateDates();
  			//$('#date-check-out').val(end.format('YYYY-MM-DD'));
	});
})