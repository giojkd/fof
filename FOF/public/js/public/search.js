var infowindow;
var map;
var estate_markers_array = new Array();

function resize_map() {
	var map_canvas = $('#map-canvas');
	var map_wrapper = map_canvas.closest('div');
	map_canvas.css({
		'position':'fixed',
		'height':$(window).height()-50,
		'width':map_wrapper.outerWidth()
	});

	if (typeof map == "undefined")
		return;
	setTimeout(function() {
		resize_map();
	}, 400);
}

$(window).resize(function(){
	resize_map();
})

$(document).ready(
	function() {
		$('.footer').remove();
	})		

var map_center = {lat: lat, lng: lng};

function create_map_first_time() {
	create_map(map_center)
}

function create_map(map_center) {

	

	var mapOptions = {
		center: map_center,
		zoom: 14
	};
	map = new google.maps.Map(document.getElementById('map-canvas'),
		mapOptions);

	var map_center_marker = new google.maps.Marker({
		position: map_center,
		map: map,
		icon: "/css/img/map-center-marker-ico.png"
	})

	google.maps.event.addListener(map_center_marker, 'click', function(evt) {
		if (infowindow) {
			infowindow.close();
		}
		infowindow = new google.maps.InfoWindow();
		infowindow = new google.maps.InfoWindow({
			content: "Questo è il centro della tua ricerca!"
		})
		infowindow.open(map, this)
	});

	add_markers(map);
	
	

	google.maps.event.addListener(map, 'click', function(event) {
		if (infowindow) {
			infowindow.close();
		}
	})
	resize_map()
	
}

google.maps.event.addDomListener(window, 'load', create_map_first_time);

function add_markers(map) {

	for (var index in estate_coordinates) {
		var estate = estate_coordinates[index];
		var estate_id = estate.id;
		estate_markers_array[estate_id] = new google.maps.Marker({
			position: {lat: parseFloat(estate.lat), lng: parseFloat(estate.lng)},
			map: map,
			title: estate.title,
			id: estate_id,
		})

		google.maps.event.addListener(estate_markers_array[estate_id], 'click', function(evt) {
			if (infowindow) {
				infowindow.close();
			}
			infowindow = new google.maps.InfoWindow();
			infowindow = new google.maps.InfoWindow({content: 'Caricamento...'
		})
			$.get('/infowindow/en_us/'+this.id, {}, function(risp) {
				infowindow.setContent(risp);
			})
			infowindow.open(map, this)
		});
	}

	$('.estate-item').hover(function() {
		var estate = $(this);
		var estate_id = $(this).attr('estate-id');
		console.log(estate_id);
		estate_markers_array[estate_id].setIcon("/css/img/map-estate-marker-red.png")
	}, function() {
		var estate = $(this);
		var estate_id = $(this).attr('estate-id');
		estate_markers_array[estate_id].setIcon("/css/img/map-estate-marker-grey.png")
	})

	
}

function watch_estate_on_map(estate_id) {
	map.panTo(estate_markers_array[estate_id].getPosition());
	google.maps.event.trigger(estate_markers_array[estate_id], 'click');
}


