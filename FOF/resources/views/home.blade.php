@extends('master')

@section('content')
<style type="text/css">
	.apartment .carousel .carousel-inner .carousel-item div{
		height:160px;
		width:100%;
	}


	#map-canvas {
	    height: 100%;
	    width: 100%;
	}
	.apartments-list{
		background-color:#ececec;
	}
	.apartment-wrapper{
		background-color:#fff;
		padding:20px;
		margin-bottom:20px;
		box-shadow: 0px 0px 4px rgba(0,0,0,0.25);
		transition: 0.5s all;
	}
	.apartment-wrapper:hover{
		background-color:#f5f5f5;
	}
	.apartment-wrapper .apartment{}

	.apartment-wrapper .apartment .apartment-description{
		font-size:0.8em;
		line-height: 1.4em;
		margin:8px 0px;
	}
	.apartment-wrapper .apartment .price{
		font-size: 1.8em;
	}

	.apartment-wrapper .apartment .price-per-month{
		font-size: 0.8em;
		color:#999;
		white-space: nowrap;
		    margin-top: -5px;
    display: -webkit-box;
	}

	.apartment-wrapper .apartment .apartment-controls{
		border:1px solid #d6d6d6;
		text-align: center;
		padding:20px;
	}
	.results-container{
		margin-top:-20px;
	}
</style>
<div class="results-container">
	<div class="row no-gutters">
		<div class="col-md-6">
                <div id="map-canvas">

                </div>
		</div>
		<div class="col-md-6 apartments-list">
			@if(count($apartments))
			@for($i=0; $i<=10; $i++)
			@foreach($apartments as $apartment)
				<div class="apartment-wrapper">
					<div class="apartment row">
						<div class="col-md-4">
							<div id="carouselApartment{{$apartment->id}}" class="carousel slide" data-ride="carousel" data-interval="false">
								<ol class="carousel-indicators">
								  	@foreach($apartment->room_photos as $index => $photo)
								    	<li data-target="#carouselApartment{{$apartment->id}}" data-slide-to="{{$index}}" class="@if($index==0) active @endif"></li>
									@endforeach
								</ol>
								<div class="carousel-inner">
									@foreach($apartment->room_photos as $index => $photo)
									  <div class="carousel-item @if($index==0) active @endif">
									    		<div style="background-image:url('/{{$photo}}')"></div>
									  </div>
									  	@endforeach
									  <a class="carousel-control-prev" href="#carouselApartment{{$apartment->id}}" role="button" data-slide="prev">
									    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
									    <span class="sr-only">Previous</span>
									  </a>
									  <a class="carousel-control-next" href="#carouselApartment{{$apartment->id}}" role="button" data-slide="next">
									    <span class="carousel-control-next-icon" aria-hidden="true"></span>
									    <span class="sr-only">Next</span>
									  </a>
								</div>
							</div>
						</div>
						<div class="col-md-8">
							<div class="row">
								<div class="col-md-12">
									<span class="h3">
										{{$apartment->room_name}}
									</span>
									<div class="row">
										<div class="col-md-7">
											<p class="apartment-description">
												{{str_limit($apartment->room_description, $limit = 150, $end = '...')}}
											</p>
											<ul class="list list-inline">
												<li class="list-inline-item">
													<span class="badge badge-info">{{$apartment->room_type_name}}</span>
												</li>
												@if($apartment->ensuite_bathroom==1)
												<li class="list-inline-item">
													<span class="badge badge-info">{{__('front.bathroom_ensuite')}}</span>
												</li>
												@endif
											</ul>
											<span class="price">{{formatPrice($apartment->price)}} </span><br/>
											<span class="price-per-month">{{__('front.per_month')}}</span>
										</div>
										<div class="col-md-5">
											<div class="apartment-controls">
												<a 
												href="<?php echo route('room_view', ['locale'=>App::getLocale(),'id' => $apartment->rooms_id]).'?'.http_build_query(['filters'=>$filters])?>" 
												class="btn btn-info btn-block" 
												><i class="fa-eye fa"></i> {{__('front.watch')}}</a>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			@endforeach
			@endfor
			@endif
		</div>
	</div>
</div>
<script type="text/javascript">
	var lat = {{str_limit($apartments[0]->apartment_lat,$limit=8,$end='')}};
	var lng = {{str_limit($apartments[0]->apartment_lng,$limit=8,$end='')}};
	var estate_coordinates = {!!$estate_coordinates_json!!};
</script>
<script src="/js/public/search.js"></script>
@endsection