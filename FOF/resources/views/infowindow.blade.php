<style>
	.infowindow-container{

	}
	.infowindow-container .ii-img-holder{
		width: 100%;
		height:200px;
		margin-bottom: 10px;
		-webkit-background-size: cover;
	  -moz-background-size: cover;
	  -o-background-size: cover;
	  background-size: cover;
	}
	.infowindow-container .ii-img .ii-overlay{
		display: block;
		position: absolute;
		
		left:0px;
		padding:10px;
		color: #fff;
		background: rgba(0,0,0,0.35);
	}
	.infowindow-container .ii-img .ii-overlay.ii-price{
		top:100px;
	}
	.infowindow-container .ii-img .ii-overlay.ii-features{
		top:140px;
	}
</style>

<div class="infowindow-container">
	<div class="ii-img">
		<div class="ii-img-holder" style="background:url('/{{$room->room_photos[0]}}')">
		<span class="ii-overlay ii-price">{{formatPrice($room->price)}}/{{__('front.month')}}</span>
		<span class="ii-overlay ii-features">
			{{$room->room_surface}}m<sup>2</sup>
		</span>

	</div>
	<div class="row">
		<div class="col-7">
			<h4>{{$room->room_name}}</h4>
			<p>{{$room->room_type}}</p>
		</div>	
		<div class="col-5">
			<a 
		href="<?php echo route('room_view', ['locale'=>App::getLocale(),'id' => $room->rooms_id]).'?'.http_build_query(['filters'=>$filters])?>" 
		class="btn btn-info btn-block" 
		><i class="fa-eye fa"></i> {{__('front.watch')}}</a>
		</div>
	</div>
	
	<p>
		{{$room->room_description}}
	</p>
	
</div>
<!--
  [id] => 1
    [created_at] => 2018-08-13 19:55:53
    [updated_at] => 
    [room_name] => stanza 1
    [room_description_en_us] => stanza privata con bagno e cucina condivisa
    [room_adults] => 2
    [room_kids] => 1
    [room_infants] => 0
    [room_beds_count] => 1
    [ensuite_bathroom] => 1
    [room_min_price] => 500
    [room_medium_price] => 600
    [room_high_price] => 700
    [room_types_id] => 2
    [apartments_id] => 6
    [room_surface] => 45
    [rooms_id] => 2
    [cms_users_id] => 18
    [apartment_name] => La pinconaia
    [apartment_description_en_us] => Il celebre pittore e scultore Pinco, ha abitato questa villa facendosi ispirare dalla vista mozzafiato
    [apartment_events_accepted] => 1
    [apartment_smoking_accepted] => 1
    [apartment_pets_accepted] => 1
    [apartment_full_address] => Piazzale Michelangelo, Firenze, FI, Italia
    [apartment_administrative_area_level_1] => Toscana
    [apartment_administrative_area_level_2] => Città Metropolitana di Firenze
    [apartment_administrative_area_level_3] => Firenze
    [apartment_locality] => Firenze
    [apartment_postal_code] => 50125
    [apartment_route] => Piazzale Michelangelo
    [apartment_street_number] => 
    [apartment_country] => Italia
    [apartment_rooms_count] => 9
    [apartment_beds_count] => 52
    [apartment_bathapartments_count] => 3
    [apartment_lat] => 43.7624623
    [apartment_lng] => 11.264897399999995
    [apartment_types_id] => 1
    [zones_id] => 
    [room_type_name_en_us] => Private room
    [apartment_type_name_en_us] => Villa
    [room_photos] => Array
        (
            [0] => uploads/1/2018-08/download_1.jpg
        )

    [service] => Array
        (
        )

    [other_rooms] => 
    [price] => 18000
    [room_description] => stanza privata con bagno e cucina condivisa
    [apartment_type] => Villa
    [room_type] => Private room
    [coordinates_json] => {"lat":43.76246,"lng":11.26489}
    -->