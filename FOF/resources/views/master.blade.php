    <!doctype html>
    <html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <title>FOF HOUSING</title>


        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.2/css/bootstrap.min.css" integrity="sha384-Smlep5jCw/wG7hdkwQ/Z5nLIefveQRIY9nfy6xoR1uRYBtpZgI6339F5dgvm/e9B" crossorigin="anonymous">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.2/js/bootstrap.min.js" integrity="sha384-o+RDsa0aLu++PJvFqy8fFScvbHFLtbvScb8AjopnFD+iEQ7wo/CG0xlczd+2O/em" crossorigin="anonymous"></script>
        <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA7_6qXx1LlpVA576SUsX9OPSdh0jw2WIo&libraries=places&language=zh-CN&region=cn"></script>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/css/bootstrap-datepicker3.css">
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/js/bootstrap-datepicker.min.js"></script>
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css" integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ" crossorigin="anonymous">
        <link rel="stylesheet" href="/css/public/style.css">
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.2/moment-with-locales.min.js"></script>
        <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
        <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
        <style>
        #daterangepicker{
            width: 110px;
        }
    </style>
</head>
<body>
    <nav class="navbar fixed-top navbar-expand-lg navbar-light bg-light">
        <a class="navbar-brand" href="/search"><img style="height:35px" src="/img/fof_logo.png"></a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav navbar-right mr-auto"><!--
                        <li class="nav-item active">
                            <a class="nav-link" href="#">Home <span class="sr-only">(current)</span></a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#">Link</a>
                        </li>
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                Dropdown
                            </a>
                            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                <a class="dropdown-item" href="#">Action</a>
                                <a class="dropdown-item" href="#">Another action</a>
                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item" href="#">Something else here</a>
                            </div>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link disabled" href="#">Disabled</a>
                        </li>-->
                    </ul>
                    <form class="form-inline my-2 my-lg-0" action="{{$search_form_action}}">
                        <label>{{__('front.search')}} </label>
                        <div class="form-group ml-2">
                            <label>
                                <select name="filters[room_types][]" class="form-control">
                                    @foreach($room_types as $room_type)
                                    <option type="checkbox"  value="{{$room_type->id}}">
                                        {{$room_type->room_type_name}}
                                    </option>op
                                    @endforeach
                                </select>
                            </label>
                        </div>
                        <div class="form-group ml-2">
                            <select name="filters[room_adults]" id="" class="form-control">
                                @for($i=1; $i<=30; $i++)
                                <option value="{{$i}}" @if($i==$filters['room_adults']) selected @endif>{{$i}} {{__('front.adults')}}</option>
                                @endfor
                            </select>
                        </div>
                        <!--
                         <div class="form-group ml-2">
                            <label>
                                <select name="filters[room_types][]" class="form-control">
                                    @foreach($zones as $zone)
                                        <option type="checkbox"  value="{{$room_type->id}}">
                                            {{$zone->zone_name}}
                                        </option>op
                                    @endforeach
                                </select>
                            </label>
                        </div>
                    -->
                    <div class="form-group ml-2">
                        <input id="daterangepicker" value="{{$filters['check_in']}} - {{$filters['check_out']}}" class="form-control" type="text">
                        <input id="date-check-in" type="hidden" name="filters[check_in]" value="{{$filters['check_in']}}">
                        <input id="date-check-out" type="hidden" name="filters[check_out]" value="{{$filters['check_out']}}">

                    </div>
                    <div class="form-group ml-2">
                        <label>
                            <select name="filters[months]" class="form-control">
                                @for($i=1; $i<=12; $i++)
                                <option type="checkbox"  value="{{$i}}" @if($filters['months']==$i) selected @endif>

                                 {{$i}} {{__('front.months')}}
                             </option>op
                             @endfor
                         </select>
                     </label>
                 </div>
                 <!--
                 <a href="" class="btn btn-outline-primary ml-2" data-toggle="modal" data-target="#filtersModal">{{__('front.more_filters')}}</a>
                 <div class="modal" id="filtersModal">
                  <div class="modal-dialog">
                    <div class="modal-content">

                      
                      <div class="modal-header">
                        <h4 class="modal-title">{{__('front.more_filters')}}</h4>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>

                    
                    <div class="modal-body d">
                        <div class="form-group">
                            <label>{{__('front.adults')}}</label>
                            <select name="filters[room_adults]" id="" class="form-control">
                                @for($i=1; $i<=30; $i++)
                                <option value="{{$i}}" @if($i==$filters['room_adults']) selected @endif>{{$i}}</option>
                                @endfor
                            </select>
                        </div>

                        <div class="form-group">
                            <label>{{__('front.rooms')}}</label>
                            <select name="filters[rooms_quantity]" id="" class="form-control">
                                @for($i=1; $i<=30; $i++)
                                <option value="{{$i}}" @if($i==$filters['rooms_quantity']) selected @endif>{{$i}}</option>
                                @endfor
                            </select>
                        </div>

                        <div class="form-group">
                            <label>{{__('front.room_type')}}</label>
                            @foreach($room_types as $room_type)
                            <label>
                                <input type="checkbox" name="filters[room_types][]" value="{{$room_type->id}}">
                                {{$room_type->room_type_name}}
                            </label>
                            @endforeach
                        </div>

                    </div>

                    
                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                    </div>

                </div>
            </div>
        </div>-->
        <button class="btn btn-outline-success ml-2" type="submit"><i class="fa fa-search"></i> {{__('front.search')}}</button>
    </form>

</div>
</nav>
<!-- The Modal -->

@yield('content')

@include('footer')
</body>
</html>
