@extends('master')

@section('content')
<style>
.required{
	color:red;
}

.room-wrapper{
padding:20px;

border:1px solid #ececec;
margin-bottom: 20px;
}

.room-wrapper img{
	width:100%;
}

.page-title{
	
	display: inline-block;
	
	margin:40px 0px;
}
</style>

<div class="container">
	<div class="row">
		<div class="col-12">
			<div class="alert alert-info">
				<i class="fa fa-gem"></i> {{__('front.reservation_almost_done')}}
			</div>
			<h2 class="page-title">{{__('front.fill_the_form_to_confirm_the_reservation')}}</h2>
		</div>
	</div>
	<form method="POST" action="/finalize_reservation">
	<div class="row">
		
		<div class="col-4">
			<h5>{{__('front.fill_with_your_data')}}</h5>
			
					{{ csrf_field() }}

				<input type="hidden" name="reservation[rooms_id]" value="{{$room->rooms_id}}">
				<input type="hidden" name="filters[check_in]" value="{{$filters['check_in']}}">
				<input type="hidden" name="filters[check_out]" value="{{$filters['check_out']}}">
				<input type="hidden" name="filters[months]" value="{{$filters['months']}}">

				<div class="form-group">
					<label>{{__('front.name')}}</label>
					<input type="text" name="user[name]" class="form-control" >
				</div>
				<div class="form-group">
					<label>{{__('front.surname')}}<span class="required">*</span></label>
					<input value="Orlandi" required type="text" name="user[surname]" class="form-control" placeholder="{{__('front.required')}}">
				</div>
				<div class="form-group">
					<label>{{__('front.address')}}</label>
					<input type="text" name="user[address]" class="form-control">
				</div>
				<div class="form-group">
					<label>{{__('front.telephone')}}<span class="required">*</span></label>
					<input value="123123123" required type="text" name="user[telephone]" class="form-control" placeholder="{{__('front.required')}}">
					<p class="form-text">{{__('front.preferably_a_cellphone')}}</p>
				</div>
				<div class="form-group">
					<label>{{__('front.email')}}<span class="required">*</span></label>
					<input value="giovanniorlandi@me.com" required type="email" name="user[email]" class="form-control" placeholder="{{__('front.required')}}">
				</div>
				<div class="form-group">
					<label>{{__('front.horoscope_sign')}}</label>
					<select name="user[horoscope_signs_id]" id="" class="form-control">
						<option value="">{{__('front.choose')}}...</option>
						@foreach($horoscope_signs as $sign)
							<option value="{{$sign->id}}">{{$sign->horoscope_sign_name}}</option>
						@endforeach
					</select>
				</div>
				<div class="form-group">
					<label>{{__('front.gender')}}</label>
					<select name="user[genders_id]" id="" class="form-control">
						<option value="">{{__('front.choose')}}...</option>
						@foreach($genders as $gender)
							<option value="{{$gender->id}}">{{$gender->gender_name}}</option>
						@endforeach
					</select>
				</div>
				<div class="form-group">
					<label>{{__('front.profession')}}</label>
					<select name="user[professions_id]" id="" class="form-control">
						<option value="">{{__('front.choose')}}...</option>
						@foreach($professions as $profession)
							<option value="{{$profession->id}}">{{$profession->profession_name}}</option>
						@endforeach
					</select>
				</div>
				
				
		
		</div>
		<div class="col-4">
			<h5>{{__('front.your_credit_card_data')}}</h5>
			<div class="form-group">
				<label>{{__('front.card_number')}}<span class="required">*</span></label>
				<input value="4242424242424242" required type="number" name="card[card_no]" class="form-control" placeholder="{{__('front.required')}}">
			</div>
			<label>{{__('front.expiration_date')}}<span class="required">*</span></label>
			<div class="form-group row">
				<div class="col-3">
					<input value="10" min="1" max="12" required type="number" name="card[ccExpiryMonth]" class="form-control" placeholder="MM">
				</div>
				<div class="col-3">
					<input value="20" required min="<?php date('y')?>" type="number" name="card[ccExpiryYear]" class="form-control" placeholder="YY">
				</div>
			</div>
			<label>{{__('front.cvv')}}<span class="required">*</span></label>
			<div class="form-group row">
				<div class="col-3">
					<input value="314" required type="number" name="card[cvvNumber]" class="form-control" placeholder="CVV">
				</div>
			</div>
		</div>
		<div class="col-4">
			<div class="room-wrapper">
				<div class="row">
					<div class="col-7">
						<h4>{{$room->room_name}}</h4>
						{{$room->room_type}}
					</div>
					<div class="col-5">
						<img src="/{{$room->room_photos[0]}}">
					</div>
				</div>
				<hr/>
				<div class="row">
					<div class="col-12">
						<i class="fa-user fa"></i> {{$filters['room_adults']}} {{__('front.guest_s')}}<br/>
						<i class="fa-calendar fa"></i> {{$filters['check_in']}}
						<i class="fa-arrow-right fa"></i>
						<i class="fa-calendar fa"></i> {{$filters['check_out']}}
					</div>
				</div>
				<hr/>
				<div class="row">
					<div class="col-8">
						{{__('front.stay')}}
						{{formatPrice($room->price)}} X {{$filters['months']}} {{__('front.months')}}
					</div>
					<div class="col-4 text-right">
						{{formatPrice($room->price_total)}}
					</div>
				</div>
				<hr/>
				@if($room->deposit_months>0)
				<div class="row">
					<div class="col-8">
						{{__('front.deposit')}}
						{{formatPrice($room->price)}} X {{$room->deposit_months}} {{__('front.months')}}
					</div>
					<div class="col-4 text-right">
						{{formatPrice($room->deposit)}}
					</div>
				</div>
				<hr/>
				@endif
					<div class="row">
					<div class="col-8">
						{{__('front.reservation_commission')}}<br/>
						22 - {{$filters['months']}} {{__('front.months')}} = {{$room->months_commission}}
					</div>
					<div class="col-4 text-right">
						{{formatPrice($room->commission_total)}}
					</div>
				</div>
				<hr/>
				
				<div class="row">
					<div class="col-8">
						{{__('front.total')}}
					</div>
					<div class="col-4 text-right">
						<b>{{formatPrice($room->total_with_commissions)}}</b>
					</div>
				</div>
			</div>
			<button class="btn btn-outline-info btn-block btn-lg">{{__('front.confirm_and_go_to_payment')}}</button>
		</div>
		
		</div>
	</form>
</div>

@endsection