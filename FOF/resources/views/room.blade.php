@extends('master')

@section('content')
<style>
.photos-outer-wrapper{
	width:100%;
	overflow: hidden;
	overflow-x:auto;
	margin:0px;
	padding:0px;
}
.photos-inner-wrapper{
	width:{{320*20*count($room->room_photos)+320}}px;
	margin:0px;
	padding:0px;
	height:320px;
}
.photos-outer-wrapper .photos-inner-wrapper .photo{
	height:320px;
	width:320px;
	display: inline-block;
	margin:0px;
	padding:0px;
	background-repeat: no-repeat;
	background-size:cover;
	margin-left:-4px;
}
#map-canvas{
	width:100%;
	height:320px;
}

#product_page_fixed_inner_nav {
    background-color: #565a5c;
    height: 50px
}
#product_page_fixed_inner_nav .nav.nav-pills li a {
    color: #cacccd;
    border-radius: 0;
    height: 50px;
    padding: 10px 15px;
    font-size: 16px
}
#product_page_fixed_inner_nav .nav.nav-pills li.active a,
#product_page_fixed_inner_nav .nav.nav-pills li:hover a {
    background: 0 0;
    color: #fff;
    border-bottom: 4px solid #cacccd
}
#product_page_fixed_inner_nav .price {
    display: block;
    color: #fff;
    font-weight: 700;
    font-size: 36px;
    background-color: #1a1b1c;
    height: 50px;
    padding-left: 10px
}

#product_page_fixed_inner_nav .price span{
	font-size: 12px;
	font-weight: normal;
	color:#fff;
}

.product_page_request_form {
    position: absolute;
    width: 100%;
    padding: 0 15px;
    top: 50px;
    left: 0;
    z-index: 9
}
.product_page_request_form>div {
    background-color: #fff;
    padding: 10px;
    border: 1px solid #dce0e0
}
#product_page_body_top {
    background: #fff;
    padding-bottom: 10px;
    
}
#room-info-wrapper{
	background-color:#f5f5f5;
	padding:60px 0px;
	font-size: 14px;
	line-height: 30px;
}
.other_room{
	border-radius: 6px;
	
}
.other_room other_room_gender{

}
.other_room other_room_title{

}
.other_room other_room_profession{

}

.room-service{

}

.room-service img{
	width: 100%
}

</style>
<script src="https://cdn.jsdelivr.net/npm/jquery.kinetic/jquery.kinetic.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/ScrollToFixed/1.0.8/jquery-scrolltofixed-min.js"></script>
<div class="photos-outer-wrapper">
	<div class="photos-inner-wrapper">
		@for($i=1; $i<=20; $i++)
		@foreach($room->room_photos as $photo)
		<div class="photo" style="background-image:URL('/{{$photo}}')"></div>
		@endforeach
		@endfor
	</div>
</div>
<div class="container-fluid">
	<div class="row">
		
	</div>
</div>


<div class="container-fluid hidden-xs hidden-sm" id="product_page_fixed_inner_nav">
	<div class="row">
		<div class="col-md-12">
			<div class="container">
				<div class="row">
					<div class="col-md-8" style="padding-top:15px">
						<ul class="nav nav-pills">
							<li class="active"><a class="intertial-scroll" href="#estate-photos">Foto</a></li>
							<li><a class="intertial-scroll" href="#estate-details">{{__('front.details')}}</a></li>
							<li><a class="intertial-scroll" href="#estate-description">{{__('front.description')}}</a></li>
							<!--<li><a class="intertial-scroll" href="#estate-agent-opinion">Secondo l'esperto</a></li>-->
							<li><a class="intertial-scroll" href="#product_page_map">{{__('front.map')}}</a></li>
						</ul>
					</div>
					<div class='col-md-4'>
						<p class='price'>
							{{formatPrice($room->price)}} <span>{{__('front.per_month')}}</span>
						</p>
						<div class='product_page_request_form'>
							<div>
								<a class="btn btn-info btn-block btn-lg" href="<?php echo route('reserve', ['locale'=>App::getLocale(),'id' => $room->rooms_id]).'?'.http_build_query(['filters'=>$filters])?>">{{__('front.reserve')}}</a>
								<hr>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<div class='container' id='product_page_body_top'>
	<div class='row'>
		<div class='col-md-8'>
			<h1>
				{{$room->room_name}}
			</h1>
			<h2>
				{{$room->full_address}}
			</h2>
		</div>
	</div>
</div>

<div id="room-info-wrapper">
<div class='container'>
	<div class="height-spacer-50"></div>
	<div class='row' id="estate-details">
		<div class='col-md-1'>
			{{__('front.details')}}
		</div>
		<div class='col-md-7'>
			<div class='row'>
				<div class='col-md-7 description-column'>
					{{__('front.apartment_type')}}: <strong>{{$room->apartment_type}}</strong><br>
					{{__('front.address')}}: <strong>{{$room->apartment_route}}, {{$room->apartment_locality}}</strong><br>
					{{__('front.room_surface')}}: <strong>{{$room->room_surface}}m<sup>2</sup></strong><br>
					{{__('front.ensuite_bathroom')}}: <strong>@if($room->ensuite_bathroom==1) {{__('front.yes')}} @else {{__('front.no')}} @endif</strong><br>
					{{__('front.room_type')}}: <strong>{{$room->room_type}}</strong><br>
					{{__('front.beds')}}: <strong>{{$room->room_beds_count}}</strong><br>
				</div>
				<div class='col-md-5 description-column'>
					{{__('front.adults')}}: <strong>{{$room->room_adults}}</strong><br>
					{{__('front.kids')}}: <strong>{{$room->room_kids}}</strong><br>
					{{__('front.infants')}}: <strong>{{$room->room_infants}}</strong><br>
					{{__('front.events_accepted')}}: <strong>@if($room->apartment_events_accepted==1) {{__('front.yes')}} @else {{__('front.no')}} @endif</strong><br>
					{{__('front.smoking_accepted')}}: <strong>@if($room->apartment_smoking_accepted==1) {{__('front.yes')}} @else {{__('front.no')}} @endif</strong><br>
					{{__('front.pets_accepted')}}: <strong>@if($room->apartment_pets_accepted==1) {{__('front.yes')}} @else {{__('front.no')}} @endif</strong><br>
				</div>
			</div>
		</div>
	</div>
	<hr>
	<div class='row' id="estate-description">
		<div class='col-md-1'>
			{{__('front.description')}}
		</div>
		<div class='col-md-7'>
			{{$room->room_description}}
		</div>
	</div>
	<hr>
	<div class='row' id="estate-occupants">
		<div class='col-md-1'>
			{{__('front.occupants')}}
		</div>
		<div class='col-md-7'>
			@if(count($room->other_rooms))
			<div class="row  no-gutters">
				@foreach($room->other_rooms as $other_room)
					<div class="col-4 other_room border border-dark">
						<?php #print_rr($other_room->reservation)?>
						<div class="row border-bottom border-dark no-gutters">
							<div class="other_room_gender col-6 text-center p-2">
								{{$other_room->room_name}}
								@if($other_room->reservation->gender_name_en_us=='Male')
									<i class="fas fa-male fa-2x"></i>
								@else
									<i class="fas fa-female"></i>
								@endif
							</div>
							<div class="other_room_title col-6 text-center text-nowrap p-2">
								{{$other_room->room_name}}
								@if($other_room->reservation->checked_in)
									<span class="badge-success badge">{{__('front.checked_in')}}</span>
								@else
									<span class="badge-warning badge">{{__('front.not_checked_in')}}</span>
								@endif
							</div>
						</div>
						<div class="row border-bottom border-dark no-gutters">
							<div class="col-6 text-center other_room_profession border-right border-dark">
								@if($other_room->reservation->profession_name!='') {{$other_room->reservation->profession_name}} @else ... @endif <br/>
								{{__('front.profession')}}
							</div>
							<div class="col-6 text-center">
								@if($other_room->reservation->horoscope_sign_name!='') {{$other_room->reservation->horoscope_sign_name}} @else ... @endif <br/>
								{{__('front.horoscope_sign')}}
							</div>
						</div>
						<div class="row no-gutters">
							<div class="col-12 text-center">
								@if($other_room->reservation->reseservation_from!='') {{$other_room->reservation->reseservation_from}} - {{$other_room->reservation->reseservation_to}} @else ... @endif<br/>
								{{__('front.occupation_period')}}
							</div>
						</div>

						
					</div>	
				@endforeach
			</div>
			@endif
		</div>
	</div>
	<hr/>
	<div class='row' id="estate-occupants">
		<div class='col-md-1'>
			{{__('front.features')}}
		</div>
		<div class='col-md-7'>
			<div class="row">
				@if(count($room->services))
					@foreach($room->services as $service)
						<div class="col-2 text-center room-service text-nowrap">
							<img src="/{{$service->service_icon}}"><br/>
							{{$service->service_name}}
						</div>
					@endforeach
				@endif
			</div>
		</div>
	</div>
</div>
</div>
<div id='product_page_map'>
    <div class='row'>
        <div class='col-md-12'>
            <div id="map-canvas">

            </div>
        </div>
    </div>
</div>




<script type="text/javascript">
	$(function(){
		$('.photos-outer-wrapper').kinetic();

		$('#product_page_fixed_inner_nav').scrollToFixed({
			marginTop: 50,
			limit: function() {
				/*var limit = $('#productpage-suggested-wrapper').offset().top-$('.product_page_request_form').outerHeight()-50-25;
				return limit;*/
			}
		});
	})

	var map_center = {!!$room->coordinates_json!!};
	function create_map() {
                var map_options = {
                    scrollwheel: false,
                    center: map_center,
                    zoom: 15
                }
                var map = new google.maps.Map(document.getElementById('map-canvas'),
                        map_options);

                var estate_marker = new google.maps.Marker({
                    position: map_center,
                    map: map
                })
            }
	  google.maps.event.addDomListener(window, 'load', create_map);
</script>
@endsection
