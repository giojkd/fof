<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Http\Request;

Route::get('/', function () {
    return redirect('search');
});

Route::get('/user', 'UserController@index');

Route::get('/search/{locale?}','SearchController@search')->name('search');

Route::get('/room/{locale}/{id}','RoomController@view')->name('room_view');

Route::get('/reserve/{locale}/{id}','ReservationController@reserve')->name('reserve');

Route::post('/finalize_reservation','ReservationController@finalizeReservation');

Route::get('/infowindow/{locale?}/{id}','Infowindow@view')->name('infowindow');
#stripe routes

Route::get('addmoney/stripe','ReservationController@payWithStripe')->name('addmoney.paywithstripe');
Route::post('addmoney/stripe','ReservationController@postPaymentWithStripe')->name('addmoney.stripe');

